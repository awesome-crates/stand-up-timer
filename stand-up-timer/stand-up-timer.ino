#include <TM1637.h>

#define CLK 2
#define DIO 3
#define BUTTON_PIN 4
#define BUZZER_PIN 5

TM1637 tm(CLK,DIO);

int countDownTimeSeconds = 20 * 60;
int buttonState = 0;

void setup() {
  // put your setup code here, to run once:
  tm.init();

  // set brightness; 0-7
  tm.set(3);

  // define the button pin as input
  pinMode(BUTTON_PIN, INPUT);
}

void loop() {
  buttonState = digitalRead(BUTTON_PIN);
  
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    countDown(countDownTimeSeconds);
  }
}

void buzz(){
  tone(BUZZER_PIN, 440, 400);
}

void displayTime(int seconds){
    int m = seconds / 60;
    int s = seconds % 60;

    tm.point(1);
    tm.display(3, s % 10);
    tm.display(2, s / 10 % 10);
    tm.display(1, m % 10);
    tm.display(0, m / 10 % 10);
}

void countDown(unsigned long seconds){
  unsigned long countdownTimeMillis = seconds*1000 + 1000;
  unsigned long startTime = millis();
  unsigned long endtTime = startTime + countdownTimeMillis;
  while(millis() <= endtTime) {
    unsigned long now = millis();
    unsigned long secondsRemaining = (countdownTimeMillis -(now - startTime))/1000;
    displayTime(secondsRemaining);
    delay(10);
  }
  buzz();
}
